﻿//set the listener
document.addEventListener("DOMContentLoaded", documentLoaded, false);

function documentLoaded(e) {
    // alert("Event: " + e.type + " - Minisweeper can start.");
    initializeGame('Intermediate');
}

function chooseLevel(e) {
    level = e.target.id;
    if (document.body.contains(document.getElementById('Minesweeper'))) {
        document.body.removeChild(document.getElementById('Minesweeper'));
    }
    initializeGame(level);
}

function newGame(level) {
    switch (level) {
        case "Intermediate":
            nrColumns = 16;
            nrRows = 16;
            nrBombs = 40;
            break;
        case "Expert":
            nrColumns = 30;
            nrRows = 16;
            nrBombs = 99;
            break;
        default:
            nrColumns = 9;
            nrRows = 9;
            nrBombs = 10;
    }
}

function initializeGame(level) {
    newGame(level);
    document.body.insertAdjacentHTML('beforeend', generateHTML(nrRows, nrColumns));  //generate dynamically a table with #nrRows of Rows and #nrColumns of Columns

    [].forEach.call(document.getElementsByTagName('input'), function (element) {
        element.addEventListener("click", chooseLevel, false);
    });
    document.getElementById('clock').innerHTML = '00:00';
    listOfSquares = document.getElementsByClassName('hidden');
    chosenSquares = chooseBombs(listOfSquares);
    document.getElementById('nrBombs').innerHTML = nrBombs;

    [].forEach.call(listOfSquares, function (element, idx) {
        element.addEventListener("mousedown", Execute, false);
        element.setAttribute('data-nrSurroundingBombs', countSurroundingBombs(idx, chosenSquares, nrColumns, nrRows));
    });

    document.oncontextmenu = function () {
        return false;
    }
}

function Execute(e) {
    var myDiv = document.getElementById(e.target.id);
    var myId = Number(myDiv.id.replace("Square", ""));
    if (e.which == 1) {
        LeftMouseButtonClicked(myId, myDiv);
    }
    if (e.which == 3) {
        RightMouseButtonClicked(myId, myDiv);
    }
}

function LeftMouseButtonClicked(myId, myDiv) {
    if (chosenSquares.includes(myId)) {
        alert('It is a bomb');
        showAllBombs(chosenSquares);
        removeEventListenerMousedown();
    } else {
        myDiv.classList.add('surround');
        if (document.getElementsByClassName("surround").length === 1) {
            timerStart();
        }
        if (myDiv.getAttribute('data-nrSurroundingBombs') === '0') {
            markNeighbours(myId);
        } else {
            myDiv.innerHTML = myDiv.getAttribute('data-nrSurroundingBombs');
        }
        checkForWin();
    }
}

function RightMouseButtonClicked(myId, myDiv) {
    myDiv.classList.add('bombExpected');
    document.getElementById('nrBombs').innerHTML = Number(document.getElementById('nrBombs').innerHTML) - 1;
}


function checkForWin() {
    if (document.getElementsByClassName("surround").length + chosenSquares.length === nrColumns * nrRows) {
        alert('You won');
        showAllBombs(chosenSquares);
        removeEventListenerMousedown();
    }
}

function removeEventListenerMousedown() {
    for (var i in listOfSquares) {
        listOfSquares[i].removeEventListener("mousedown", Execute, false);
    }
}



//---------------------Begin generateBombs-------------------------------------------------
function showAllBombs(chosenSquares) {
    clearTimeout(t);
    for (var j in chosenSquares) {
        listOfSquares[chosenSquares[j]].classList.add('bomb');
    }
}

function chooseBombs(listOfSquares) {
    var arrLength = listOfSquares.length - 1;
    var chosenSquares = [];
    while (chosenSquares.length < nrBombs) {
        var squareId = getRandomInt(0, arrLength);
        if (!chosenSquares.includes(squareId)) {
            chosenSquares.push(squareId);
        }
    }
    return chosenSquares;
}


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//---------------------End generateBombs-------------------------------------------------


//---------------------Begin count and mark SurroundingBombs-------------------------------------------------
function countSurroundingBombs(squareId, chosenSquares, nrColumns, nrRows) {
    if (chosenSquares.indexOf(Number(squareId)) === -1) {
        var listNeighbours = chooseNeighbours(squareId, nrColumns, nrRows)
        var listSurroundingBombs = listNeighbours.filter((item) => { return chosenSquares.includes(item) });
        return listSurroundingBombs.length;
    }
    else {
        return -1;
    }
}

function chooseNeighbours(squareId, nrColumns, nrRows) {
    var temp = [(Number(Number(squareId) - nrColumns)), (Number(Number(squareId) + nrColumns))];
    var listNeighbours = [];
    if (Number(squareId) % nrColumns != 0) {
        temp.push((Number(Number(squareId) - nrColumns - 1)));
        temp.push((Number(Number(squareId) - 1)));
        temp.push((Number(Number(squareId) + nrColumns - 1)));
    }
    if ((Number(squareId) + 1) % nrColumns != 0) {
        temp.push((Number(Number(squareId) - nrColumns + 1)));
        temp.push((Number(Number(squareId) + 1)));
        temp.push((Number(Number(squareId) + nrColumns + 1)));
    }
    for (var i in temp) {
        if (temp[i] >= 0 && temp[i] < nrRows * nrColumns) {
            listNeighbours.push(temp[i]);
        }
    }
    return listNeighbours;
}

function markNeighbours(squareId) {
    var listNeighbours = chooseNeighbours(squareId, nrColumns, nrRows);
    if (!listOfSquares[squareId].classList.contains('bombExpected')) {
        listOfSquares[squareId].classList.add('surround');
        if (listOfSquares[squareId].getAttribute('data-nrSurroundingBombs') !== '0') {
            listOfSquares[squareId].innerHTML = listOfSquares[squareId].getAttribute('data-nrSurroundingBombs');
        }
    }
    var toBeSurrounded = markDirectNeighbours(listNeighbours, []);
    while (toBeSurrounded.length > 0) {
        toBeSurrounded = markDirectNeighbours(chooseNeighbours(toBeSurrounded[0], nrColumns, nrRows), toBeSurrounded);
        if (!listOfSquares[toBeSurrounded[0]].classList.contains('bombExpected')) {
            listOfSquares[toBeSurrounded[0]].classList.add('surround');
        }
        toBeSurrounded.shift();
    }
}


function markDirectNeighbours(listNeighbours, toBeSurrounded) {
    for (var i in listNeighbours) {
        if (listOfSquares[listNeighbours[i]].getAttribute('data-nrSurroundingBombs') !== '0') {
            if (!listOfSquares[listNeighbours[i]].classList.contains('bombExpected')) {
                listOfSquares[listNeighbours[i]].classList.add('surround');
                listOfSquares[listNeighbours[i]].innerHTML = listOfSquares[listNeighbours[i]].getAttribute('data-nrSurroundingBombs');
            }
        } else {
            if (!listOfSquares[listNeighbours[i]].classList.contains('surround')) {
                toBeSurrounded.push(listNeighbours[i]);
            }
        }
    }
    return toBeSurrounded;
}
//---------------------End count and mark SurroudingBombs-------------------------------------------------

//---------------------Begin-generateHTML----------------------------------------------------
//generate dynamically a table with #nrRows of Rows and #nrColumns of Columns
//each Square in the table is of class hidden and has a unique id
function generateHTML(nrRows, nrColumns) {
    return '<div id="Minesweeper"> <div> <h2>Minesweeper</h2></div><div id="gameBoard"><div id=Container>' + generateBoard(nrRows, nrColumns) + '</div>' + generateButtons() + '</div></div>';

}

function generateClockAndBombCounter() {
    return '<div id="row0"><div id=nrBombs></div><div id="clock"></div></div>';
}


function generateButton(btnId, btnValue) {
    return '<input type="button" id= "' + btnId + '" class="button" value="' + btnValue + '" />';
}

function generateButtons() {
    return '<div id= "containerButtons">' + generateButton('Beginner', 'Beginner') + generateButton('Intermediate', 'Intermediate')
        + generateButton('Expert', 'Expert') + generateClockAndBombCounter() + '</div>';

}

function generateBoard(nrRows, nrColumns) {
    var containerBoard = '<div>';
    for (var rowId = 1; rowId <= nrRows; rowId++) {
        containerBoard += '<div id="row' + rowId + '">' + generateRow(rowId, nrColumns) + '</div>';
    }
    return containerBoard + '</div>';
}

function generateRow(rowId, nrColumns) {
    var rowSquares = '';
    for (var columnId = 1; columnId <= nrColumns; columnId++) {
        rowSquares += '<div id="' + generateIdForSquareElement(rowId, columnId, nrColumns) + '" class="hidden"> </div>';
    }
    return rowSquares;
}

function generateIdForSquareElement(rowId, columnId, nrColumns) {
    var id = (Number(rowId) - 1) * Number(nrColumns) + Number(columnId) - 1;
    return 'Square' + String(id);
}
//---------------------End-generateHTML----------------------------------------------------

//---------------------start timer-------------------------------------------------
function timerStart() {
    myClock = document.getElementById('clock');
    myClock.innerHTML = '00:00';
    seconds = 0, minutes = 0;
    timer();
}

function addTime() {
    seconds++;
    if (seconds >= 60) {
        seconds = 0;
        minutes++;
        if (minutes >= 60) {
            alert('you play longer than one hour');
        }
    }
    myClock.innerHTML = String(checkTime(minutes) + ':' + checkTime(seconds));
    timer();
}

function timer() {
    t = setTimeout(addTime, 1000);
}

function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

//---------------------end timer-------------------------------------------------